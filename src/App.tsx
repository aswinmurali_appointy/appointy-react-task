import Card from './Components/Card/Card';

import Header from './Sections/Header';
import Footer from './Sections/Footer';
import { useState } from 'react';

enum CardState {
  free,
  pro,
  enterprise,
}

const App = () => {
  const [selected, setSelected] = useState(CardState.free);

  function focusToFree() { return setSelected(CardState.free) };
  function focusToPro() { return setSelected(CardState.pro) };
  function focusToEnterprise() { return setSelected(CardState.enterprise) };

  return (
    <div className='app'>
      <header>
        <Header />
        <table className='cardsview' style={{
          verticalAlign: 'bottom',
          margin: '0 auto',
        }}>
          <th>
            <Card
              heading='Free'
              description=''
              pricing={0}
              isFocus={selected === CardState.free}
              buttonText='SIGN UP FOR FREE'
              focusCallback={focusToFree}
              points={[
                '10 users included',
                '2 GB of storage',
                'Help center access',
                'Email support',
              ]} />
          </th>
          <th>
            <Card
              heading='Pro'
              description='Most Popular'
              pricing={15}
              isFocus={selected === CardState.pro}
              buttonText='GET STARTED'
              focusCallback={focusToPro}
              points={[
                '20 users included',
                '10 GB of storage',
                'Help center access',
                'Priority Email support',
              ]} />
          </th>
          <th>
            <Card
              heading='Enterprise'
              description=''
              pricing={30}
              isFocus={selected === CardState.enterprise}
              buttonText='CONTACT US'
              focusCallback={focusToEnterprise}
              points={[
                '50 users included',
                '30 GB of storage',
                'Help center access',
                'Photo & email support',
              ]} />
          </th>
        </table>
        <Footer />
      </header>
    </div>
  );
}

export default App;
